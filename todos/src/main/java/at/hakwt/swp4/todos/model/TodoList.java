package at.hakwt.swp4.todos.model;

import java.util.ArrayList;
import java.util.List;

public class TodoList {

    private String name;

    private List<Todo> todos;

    /**
     * A todolist must have a name
     * @param name
     */
    public TodoList(String name) {
        this.name = name;
        this.todos = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void addTodo(String description) {
        if ( description == null || description.isEmpty() ) {
            throw new IllegalArgumentException("Cannot create a todo with empty description");
        }
        todos.add(new Todo(description));
    }

    public List<Todo> getTodos() {
        return todos;
    }
}
