package at.hakwt.swp4.todos.model;

public class Todo {

    private String description;

    private boolean finished;

    private User assignedUser;

    /**
     * When a Todo is created, it must have a description. By default
     * it is not finished and it does not have an assigned user
     *
     * @param description
     */
    public Todo(String description) {
        this.description = description;
        this.finished = false;
        this.assignedUser = null;
    }

    public void assign(User user) {
        this.assignedUser = user;
    }

    public User getAssignedUser() {
        return assignedUser;
    }

    public void finish() {
        if ( assignedUser == null ) {
            throw new IllegalStateException("A user must be assigned before a todo can be finished");
        }
        this.finished = true;
    }

    public String getDescription() {
        return description;
    }

    public boolean isFinished() {
        return finished;
    }
}
