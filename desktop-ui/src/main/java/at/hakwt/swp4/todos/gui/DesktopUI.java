package at.hakwt.swp4.todos.gui;

import at.hakwt.swp4.todos.model.TodoList;

import javax.swing.*;
import java.awt.*;


public class DesktopUI extends JFrame {

    public DesktopUI(TodoList todoList) {
        super("Todo List");

        JPanel panel = new JPanel();

        JLabel label = new JLabel(todoList.getName());
        panel.add(label);

        TodoTableModel todoTableModel = new TodoTableModel(todoList.getTodos());

        JTable table = new JTable(todoTableModel);
        JScrollPane jScrollPane = new JScrollPane(table);
        jScrollPane.setPreferredSize(new Dimension(400, 120));

        panel.add(jScrollPane);

        add(panel);
        setVisible(true);
        setSize(500, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
}
