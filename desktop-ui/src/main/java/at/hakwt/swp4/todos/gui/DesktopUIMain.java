package at.hakwt.swp4.todos.gui;

import at.hakwt.swp4.todos.model.TodoList;
import at.hakwt.swp4.todos.model.User;

public class DesktopUIMain {

    public static void main(String[] args) {
        TodoList todoList = new TodoList("SWP Projektarbeit");
        todoList.addTodo("Datenbank erstellen");
        todoList.addTodo("Datenzugriffslogik implementieren");
        todoList.addTodo("Userinterface implementieren");
        todoList.getTodos().get(0).assign(new User("Leonie"));
        todoList.getTodos().get(1).assign(new User("Nicole"));
        todoList.getTodos().get(2).assign(new User("Marlene"));
        new DesktopUI(todoList);
    }

}
