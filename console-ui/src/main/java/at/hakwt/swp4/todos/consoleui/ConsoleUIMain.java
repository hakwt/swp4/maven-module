package at.hakwt.swp4.todos.consoleui;

import at.hakwt.swp4.todos.model.Todo;
import at.hakwt.swp4.todos.model.TodoList;
import at.hakwt.swp4.todos.model.User;

public class ConsoleUIMain {

    public static void main(String[] args) {
        TodoList todoList = new TodoList("SWP Projektarbeit");
        todoList.addTodo("Datenbank erstellen");
        todoList.addTodo("Datenzugriffslogik implementieren");
        todoList.addTodo("Userinterface implementieren");
        todoList.getTodos().get(0).assign(new User("Leonie"));
        todoList.getTodos().get(1).assign(new User("Nicole"));
        todoList.getTodos().get(2).assign(new User("Marlene"));

        System.out.println(todoList.getName());
        String format = "%-10s%-20s%s%n";
        System.out.printf(format, "erledigt?", "Wer?", "Was?");

        for(Todo todo : todoList.getTodos()) {
            String finished = "o";
            if ( todo.isFinished() ) {
                finished = "x";
            }
            System.out.printf(format, finished, todo.getAssignedUser().getName(), todo.getDescription());
        }

    }

}
